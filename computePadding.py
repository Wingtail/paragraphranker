import tensorflow as tf


a = tf.constant([[[1.0,2.0,4.5],[0.,0.,0.],[5.,3.,5.7]],[[0.,0.,0.],[0.,0.,0.],[5.,3.,5.7]],[[1.,2.,3.],[0.,0.,0.],[5.,3.,5.7]]])
b = tf.reduce_sum(a, axis=2)
c = tf.cast(tf.math.equal(b,0.0), dtype=tf.float32)
c = tf.expand_dims(c, axis=-1)

shape = c.get_shape().as_list()
shape[len(shape)-1] = 300
print(shape)
d = tf.tile(c, [1,1,300])
e = tf.slice(tf.reduce_sum(d, axis=1),[0,0],[-1,1])

pos = tf.reduce_sum(a, axis=1)
pos = tf.divide(pos,e)


with tf.Session() as session:
    a,b,c,d,e,pos = session.run([a,b,c,d,e, pos])
    print(a)
    print(b)
    print(c)
    print(d)
    print(e)
    print(pos)
