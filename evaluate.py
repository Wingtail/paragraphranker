import tensorflow as tf
import pickle
from tensorflow import contrib
import numpy as np
import math
from transformerBlock import Transformer, ValidationSiamese
import random
from datetime import datetime
from tensorflow import keras
from tensorflow.python.keras.callbacks import TensorBoard
from dataGen import ValidationDataGenerator
import gzip
from tensorflow.python.keras.callbacks import LearningRateScheduler

transformer = Transformer(3, 10, 300)
transformer.load_weights('./checkpoint/transformer_best')
model = Siamese(transformer, 300, 300)


model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1.0e-6), loss='mse', metrics=['mae'])

def parse(record):
    # Don't forget to replace the shape with the actual value
    features = {
        'sentence1': tf.FixedLenFeature([300], tf.int64),
        'sentence2': tf.FixedLenFeature([300], tf.int64),
        'label': tf.FixedLenFeature([1], tf.float32)
    }
    parsed_example = tf.parse_single_example(record, features)
    return ({'sentence1':parsed_example['sentence1'],'sentence2':parsed_example['sentence2']}, parsed_example['label'])


dataset = tf.data.TFRecordDataset('valiSentencePairs.tfrecord')
dataset = dataset.map(parse)
#dataset = dataset.repeat()
dataset = dataset.shuffle(int(300*0.4) + 3 * 64)
#dataset = dataset.batch(64)
training_iterator = dataset.make_one_shot_iterator()

results = model.predict(training_iterator, steps=10)

size = len(results)
for i in range(size):
    if(results[i] >= 0.25):
        results[i] = 0.0
    else:
        results[i] = 1.0

a = np.sum(results) / float(size)

#print(results)
print(size)
print(a)
