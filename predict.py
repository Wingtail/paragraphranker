from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from datetime import datetime
from packaging import version
from tensorflow.python.keras.callbacks import TensorBoard
import math
from transformerBlock import Transformer, Siamese
import pickle
import re
import nltk

transformer = Transformer(3,10,300)
transformer.load_weights('./checkpoint/transformer_best')

grouping = pickle.load(open("words.dat", "rb"))

vocabulary = grouping[0]
lookuptable = grouping[1]

sess = tf.Session()

def sentToVec(sent1):
    sent1 = sent1.lower()
    words1 = nltk.word_tokenize(sent1)
    words1 = [word for word in words1 if len(word) > 0]

    sentIndex = []
    for word in words1:
        sentIndex.append(vocabulary[word])

    sentData = np.zeros((1, 300, len(lookuptable[0])))

    sent = np.asarray([lookuptable[index] for index in sentIndex])
    sentData[0][:sent.shape[0],:sent.shape[1]] = sent
    prediction = transformer.predict(sentData)
    return prediction

while(True):
    sent1 = input("sentence: ")

    sent2 = input("sentence: ")

    vec1 = sentToVec(sent1)
    vec2 = sentToVec(sent2)

    dotted = tf.math.multiply(vec1, vec2)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vec1), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vec2), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom).eval(session = sess)

    print('sim: ',cosineSim)
