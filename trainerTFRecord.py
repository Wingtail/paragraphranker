from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from datetime import datetime
from packaging import version
from tensorflow.python.keras.callbacks import TensorBoard
from tensorflow.python.keras.callbacks import LearningRateScheduler
from tensorflow.keras.preprocessing.sequence import pad_sequences
import math
from transformerBlock import Transformer, Siamese
from tensorflow.keras.layers import Embedding

#from dataLoader import Data, DataLoader
import os.path
import pickle
import gzip
#from dataGen import DataGenerator


#train1 = np.memmap('./trainingSentence1.npy', dtype='float', mode='r', shape=(431948,248,300))
#train2 = np.memmap('./trainingSentence2.npy', dtype='float', mode='r', shape=(431948,248,300))
#trainTarget = np.memmap('./trainingTarget.npy', dtype='float', mode='r', shape=(431948))
#vali1 = np.memmap('./validationSentence1.npy', dtype='float', mode='r', shape=(18805,248,300))
#vali2 = np.memmap('./validationSentence2.npy', dtype='float', mode='r', shape=(18805,248,300))
#valiTarget = np.memmap('./validationTarget.npy', dtype='float', mode='r', shape=(18805))

#loader = DataLoader()
#data = loader.loadData()

##train1 = data.sent1Data
# train2 = data.sent2Data
#
# vali1 = data.vali1Data
# vali2 = data.vali2Data
# valiTarget = data.valiTarget
#
# trainTarget = data.trainTarget

# grouping = pickle.load(open("words.dat","rb"))
# lookuptable = grouping[1]
#
with gzip.open('vali1.pgz','r') as file:
    vali1 = pickle.load(file)
    vali1 = pad_sequences(vali1, 300, padding='post')

with gzip.open('vali2.pgz','r') as file:
    vali2 = pickle.load(file)
    vali2 = pad_sequences(vali2, 300, padding='post')

with gzip.open('valiLabel.pgz','r') as file:
    valiLabels = pickle.load(file)

maximum = 300

grouping = pickle.load(open("words.dat","rb"))
lookuptable = grouping[1]

logdir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboardCallback = keras.callbacks.TensorBoard(log_dir=logdir)

print('maximum: ',maximum)

validataset = tf.data.Dataset.from_tensor_slices(({"sentence1":vali1, "sentence2":vali2}, valiLabels))
validataset = validataset.batch(64)
#
transformer = Transformer(3, 10, 300)
#transformer.load_weights('./checkpoint/transformer_best')
model = Siamese(transformer, maximum, 300, lookuptable, 300)

# def parse(record):
#     sequenceFeatures = {'sentence1': tf.FixedLenSequenceFeature([maximum], dtype=tf.int64),
#                         'sentence2': tf.FixedLenSequenceFeature([maximum], dtype=tf.int64),
#                         'target': tf.FixedLenSequenceFeature([1], dtype=tf.float32)
#     }
#     parsed_example = tf.io.parse_single_sequence_example(
#         record,
#         sequence_features=sequenceFeatures)
#
#     return parsed_example[1]['sentence1'], parsed_example[1]['sentence2']

def schedule(epoch):
    if(epoch < 1):
        lr = 1.0e-4
    else:
        lr = 1.0e-4 * math.exp(-(float(epoch)))
    print(lr)
    return lr
callback = LearningRateScheduler(schedule)

def parse(record):
    # Don't forget to replace the shape with the actual value
    features = {
        'sentence1': tf.FixedLenFeature([300], tf.int64),
        'sentence2': tf.FixedLenFeature([300], tf.int64),
        'label': tf.FixedLenFeature([1], tf.float32)
    }
    parsed_example = tf.parse_single_example(record, features)
    return ({'sentence1':parsed_example['sentence1'],'sentence2':parsed_example['sentence2']}, parsed_example['label'])

dataset = tf.data.TFRecordDataset('trainSentencePairs.tfrecord')
dataset = dataset.map(parse)
dataset = dataset.repeat()
dataset = dataset.shuffle(int(300*0.4) + 3 * 64)
dataset = dataset.batch(64)
training_iterator = dataset.make_one_shot_iterator()

# validataset = tf.data.TFRecordDataset('valiSentencePairs.tfrecord')
# validataset = validataset.map(parse)
# validataset = validataset.shuffle(int(300*0.4) + 3 * 64)
# validataset = validataset.batch(64)


# validation_iterator = validataset.make_one_shot_iterator()

model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1.0e-4), loss='mae', metrics=['acc'])

model.fit(training_iterator, validation_data=validataset, steps_per_epoch=13284, epochs=10, callbacks=[tensorboardCallback, callback])

transformer.save_weights('./checkpoint/transformer_learnVec')

#print('transferred sentence1 shape: ',sentences1.shape) 2
#print('transferred sentence2 shape: ',sentences2.shape)

# ep = 5
# count = 0
#
# def schedule(epoch):
#     if(epoch < 1):
#         lr = 1.0e-4
#     else:
#         lr = 1.0e-4 * math.exp(-(float(count+epoch)*8.0/5))
#     print(lr)
#     return lr
#
# callback = LearningRateScheduler(schedule)
#
# for epch in range(ep):
#     print('epoch: ', epch)
#     lr = schedule(0)
#     count += 1
#     for i in range(3):
#         print('learning rate: ', lr)
#         with gzip.open('train1_'+str(i)+'.pgz','r') as file:
#             sent1 = pickle.load(file)
#             # sent1 = pad_sequences(sent1, 300, padding='post')
#
#         with gzip.open('train2_'+str(i)+'.pgz','r') as file:
#             sent2 = pickle.load(file)
#             # sent2 = pad_sequences(sent2, 300, padding='post')
#
#         with gzip.open('trainLabel_'+str(i)+'.pgz','r') as file:
#             labels = pickle.load(file)
#
#         model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lr), loss='mae', metrics=['acc'])
#
#         #X = np.array([[[0., 0.], [0., 0.], [1, 0], [0, 1], [0, 1]],
#                       #[[0., 0.], [0, 1], [1, 0], [0, 1], [0, 1]]], dtype=np.float)s
#
#         # slider = 0
#         # params = {'dim': (500,300),
#         #           'batch_size': 64,
#         #           'dataDirectory':"trainSentencePairs.pgz",
#         #           }
#         #
#         # trainGen = DataGenerator(**params)
#         # params = {'dim': (500,300),
#         #           'batch_size': 64,
#         #           'dataDirectory':"valiSentencePairs.pgz",
#         #           }
#         # valiGen = DataGenerator(**params)
#
#         print('loading dataset..')
#         print(sent1)
#         dataset = tf.data.Dataset.from_tensor_slices(({"sentence1":sent1, "sentence2":sent2}, labels))
#         dataset = dataset.batch(64)
#         #dataset = dataset.repeat()
#         dataset = dataset.shuffle(buffer_size= (int(sent1.shape[0]*0.4) + 3 * 64))
#         print('loaded')
#         if(i == 2):
#             validataset = validataset.shuffle(buffer_size= (int(vali1.shape[0]*0.4) + 3 * 64))
#             model.fit(dataset, validation_data=validataset, epochs=2, callbacks=[tensorboardCallback, callback]) #,
#         else:
#             model.fit(dataset, validation_data=validataset, epochs=2, callbacks=[tensorboardCallback, callback]) #,
#         print('saving')
#         transformer.save_weights('./checkpoint/transformer_best')
#
# print('done')
#
# # print('loading')
#
# # transformer = Transformer(3,6,300)
# # transformer.load_weights('./checkpoint/transformerr')
# #
# # model = Siamese(transformer)
# # model.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='mse', metrics=['acc'])
# # print('evaluating...')
# #
# # loss, acc = model.evaluate(dataset)
# # print(transformer.predict(sentences1))
# #
# # print('restored model: acc: ',acc, 'loss: ',loss)
#
#
# #print(model.summary())
# #print("prediction!!: ",model.predict([data, data2]))
