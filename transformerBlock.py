from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from datetime import datetime
from packaging import version
from tensorflow.python.keras.callbacks import TensorBoard
import math
from tensorflow.keras.layers import Embedding


# class SelfAttention(tf.keras.layers.Layer):
#     def __init__(self, dk, **kwargs):
#         self.dk = dk
#         super(SelfAttention, self).__init__(**kwargs)
#
#     def build(self, inputShape):
#         shape = tf.TensorShape((inputShape[2], self.dk)) #considering input as batch dimension
        # self.Q = self.add_weight(name='Q', shape=shape, initializer='uniform', trainable=True)
        # self.K = self.add_weight(name='K', shape=shape, initializer='uniform', trainable=True)
        # self.V = self.add_weight(name='V', shape=shape, initializer='uniform', trainable=True)
#         super(SelfAttention, self).build(inputShape)
#
#
#     def call(self, inputs, mask=None):
#         q = tf.matmul(inputs, self.Q)
#         k = tf.matmul(inputs, self.K)
#         v = tf.matmul(inputs, self.V)
#         kT = tf.transpose(k, perm=[0,2,1])
#         attention = tf.divide(tf.matmul(q,kT), tf.math.sqrt(float(self.dk)))
#
#         #shape = attention.get_shape().as_list()
#         #shape[0] = -1
#         selfAttentionMask = mask * -1e9
#
#         attention = tf.add(attention, selfAttentionMask)
#         attention = tf.nn.softmax(attention)
#         self_attention = tf.matmul(attention, v)
#         return self_attention
#
#     def compute_output_shape(self, inputShape):
#         shape = tf.TensorShape(inputShape).as_list()
#         shape[-1] = self.dk
#         return tf.TensorShape(shape)

class Feedforward(tf.keras.layers.Layer):
    def __init__(self, outDim, **kwargs):
        super(Feedforward, self).__init__(**kwargs)
        self.outDim = outDim

    def build(self, inputShape):
        # shape = tf.TensorShape((inputShape[2], self.outDim))
        self.dense1 = tf.keras.layers.Dense(self.outDim, activation=tf.nn.relu, name='Dense1')
        self.dense2 = tf.keras.layers.Dense(self.outDim, name='Dense2')
        # self.weight1 = self.add_weight(name='W1', shape=shape, initializer='random_normal', trainable=True)
        # self.weight2 = self.add_weight(name='W2', shape=shape, initializer='random_normal', trainable=True)
        # shape = tf.TensorShape((inputShape[1], self.outDim))
        # self.bias1 = self.add_weight(name='bias1', shape=shape, initializer='zeros', trainable=True)
        # self.bias2 = self.add_weight(name='bias2', shape=shape, initializer='zeros', trainable=True)
        super(Feedforward, self).build(inputShape)

    def call(self, inputs, mask=None):
        linear1 = self.dense1(inputs)
        #linear1 = tf.multiply(linear1, mask) #mask is posMask: 0 --> padding, 1 --> not padding
        linear2 = self.dense2(linear1)
        #linear2 = tf.multiply(linear2, mask)
        return linear2

class Multihead(tf.keras.layers.Layer):
    def __init__(self, h, outDim, **kwargs):
        super(Multihead, self).__init__(**kwargs)
        self.outDim = outDim

        self.h = h #number of heads

    def build(self, inputShape):
        shape = tf.TensorShape((self.outDim, self.outDim)) #assume first dimension is batch
        self.Q = tf.keras.layers.Dense(self.outDim, use_bias=False, name='q')
        self.K = tf.keras.layers.Dense(self.outDim,use_bias=False, name='k')
        self.V = tf.keras.layers.Dense(self.outDim,use_bias=False, name='v')
        self.linear = tf.keras.layers.Dense(self.outDim, use_bias=False, name='concat_dense')
        # self.selfAttentions = []
        # self.dk = int(self.outDim / self.h)
        # for i in range(self.h):
        #     self.selfAttentions.append(SelfAttention(self.dk))
        super(Multihead, self).build(inputShape)

    def split_heads(self, x):
        batch_size = tf.shape(x)[0]
        length = tf.shape(x)[1]

        # Calculate depth of last dimension after it has been split.
        depth = int(self.outDim / self.h)

        # Split the last dimension
        x = tf.reshape(x, [batch_size, length, self.h, depth])

        # Transpose the result
        return tf.transpose(x, [0, 2, 1, 3])

    def combine_heads(self, x):
        batch_size = tf.shape(x)[0]
        length = tf.shape(x)[2]
        x = tf.transpose(x, [0, 2, 1, 3])  # --> [batch, length, num_heads, depth]
        return tf.reshape(x, [batch_size, length, self.outDim])

    def call(self, inputs, mask=None):
        q = self.Q(inputs)
        k = self.K(inputs)
        v = self.V(inputs)

        q = self.split_heads(q)
        k = self.split_heads(k)
        v = self.split_heads(v)

        depth = (self.outDim / self.h)
        q *= depth ** -0.5

        logits = tf.matmul(q, k, transpose_b=True)
        logits += (tf.expand_dims(mask,1) * -1e9)
        weights = tf.nn.softmax(logits)
        attention_output = tf.matmul(weights, v)

        attention_output = self.combine_heads(attention_output)
        encoded = self.linear(attention_output)
        #encoded = tf.multiply(encoded, mask[1])
        return encoded

    def compute_output_shape(self, inputShape, mask=None):
        shape = tf.TensorShape(inputShape).as_list()
        shape[-1] = self.outDim
        return tf.TensorShape(shape)

class TransformerLayer(tf.keras.layers.Layer):
    def __init__(self, h, outDim, **kwargs):
        super(TransformerLayer, self).__init__(**kwargs)
        self.h = h
        self.outDim = outDim
        self.multihead = Multihead(h, outDim)
        self.feedforward = Feedforward(outDim)
        self.norm1 = tf.keras.layers.LayerNormalization(center=True, axis=2,beta_initializer=tf.keras.initializers.zeros, trainable=True)
        self.norm2 = tf.keras.layers.LayerNormalization(center=True, axis=2,beta_initializer=tf.keras.initializers.zeros, trainable=True)

    def call(self, inputs, mask=None):
        encoded = self.multihead(inputs, mask=mask)
        residual = tf.add(encoded, inputs)
        normed1 = self.norm1(residual)
        transformed = self.feedforward(normed1)
        residual2 = tf.add(transformed, normed1)
        normed2 = self.norm2(residual2)
        return normed2


class Transformer(tf.keras.Model):
    def __init__(self, N, h, outDim):
        super(Transformer, self).__init__(name='Transformer')
        self.outDim = outDim
        self.N = N
        self.transformerLayers = []
        for i in range(N):
            self.transformerLayers.append(TransformerLayer(h, outDim))

        self.wordDimVals = [10000**(2*k/float(outDim)) for k in range(outDim)]
        self.posMat = None
        self.dense = tf.keras.layers.Dense(outDim, activation=tf.nn.tanh)
        self.dropout = tf.keras.layers.Dropout(0.1)
        self.flatten = tf.keras.layers.Flatten()

    def createMask(self, x):
        b = tf.reduce_sum(tf.math.abs(x), axis=2)
        c = tf.cast(tf.math.equal(b,0.0), dtype=tf.float32) #1 -> padding, 0 -> not padding
        mask = tf.expand_dims(c, axis=1)
        return mask

    def posMask(self, x):
        b = tf.reduce_sum(tf.math.abs(x), axis=2)
        c = tf.cast(tf.math.logical_not(tf.math.equal(b,0.0)), dtype=tf.float32) #0 -> padding, 1 -> not padding
        mask = tf.expand_dims(c, axis=-1)

        #mask = tf.tile(c, [1,1,x.get_shape().as_list()[2]])
        #print("maskShape: ", mask.shape)
        #mask = tf.cast(tf.math.equal(x,0.0), tf.float32)
        return mask

    def createPosMat(self, x):
        self.posMat = np.zeros((x.shape[1].value, x.shape[2].value), dtype=np.float32)
        for i in range(x.shape[1].value):
            for j in range(x.shape[2].value):
                if(j%2==0):
                    self.posMat[i][j] = math.sin(i/float(self.wordDimVals[j]))
                else:
                    self.posMat[i][j] = math.cos(i/float(self.wordDimVals[j]))

    def call(self, inputs, training=False):
        x = inputs
        mask = self.createMask(x)
        if(self.posMat is None):
            self.createPosMat(x)
        posMask = self.posMask(x)
        print('shape: ',self.posMat)
        posMat = tf.multiply(posMask,self.posMat)
        x = tf.add(x, posMat)

        for i in range(self.N):
            x = self.transformerLayers[i](x, mask=mask)
        #sentLength = tf.reduce_sum(posMask, axis=1) #use posMask since 0->padding, 1-> not padding
        #vector = tf.math.reduce_sum(x, axis=1)
        x = self.flatten(x)
        #print(training)
        if(training == True):
            x = self.dropout(x, training=training)
        vector = self.dense(x)
        #vector = tf.divide(vector,sentLength)
        return vector

def Siamese(transformerLayer, maximum, inputDim, embeddingWeights, inputLength):
    sentence1 = tf.keras.Input(shape=(inputDim), name='sentence1')
    sentence2 = tf.keras.Input(shape=(inputDim), name='sentence2')

    embedding = Embedding(len(embeddingWeights), len(embeddingWeights[0]), weights=[embeddingWeights], input_length=inputLength, trainable=False)

    convsentence1 = embedding(sentence1)
    convsentence2 = embedding(sentence2)

    vector1 = transformerLayer(convsentence1)
    vector2 = transformerLayer(convsentence2)

    dotted = tf.math.multiply(vector1, vector2)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector1), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector2), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom)
    model = tf.keras.Model(inputs=[sentence1, sentence2], outputs = [cosineSim])
    model.summary()
    return model

def ValidationSiamese(transformerLayer, maximum, inputDim):
    sentence1 = tf.keras.Input(shape=(maximum,inputDim), name='sentence1', dtype=tf.float32)
    sentence2 = tf.keras.Input(shape=(maximum,inputDim), name='sentence2', dtype=tf.float32)
    target = tf.keras.Input(shape=(1),name='target', dtype=tf.float32)

    vector1 = transformerLayer(sentence1)
    vector2 = transformerLayer(sentence2)

    dotted = tf.math.multiply(vector1, vector2)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector1), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector2), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom)
    sim = tf.abs(tf.subtract(cosineSim, target))
    model = tf.keras.Model(inputs=[sentence1, sentence2, target], outputs = [sim])
    model.summary()
    return model

def ParagraphSiamese(transformerLayer, maximum):
    paragraph = tf.keras.Input(shape=(maximum,transformerLayer.outDim), name='paragraph', dtype=tf.float32)
    queries = tf.keras.Input(shape=(maximum,transformerLayer.outDim), name='queries', dtype=tf.float32)

    vector = transformerLayer(paragraph)
    matrix = transformerLayer(queries)

    dotted = tf.math.multiply(vector, matrix)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(matrix), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom)
    model = tf.keras.Model(inputs=[paragraph, queries], outputs = [cosineSim])
    model.summary()
    return model
