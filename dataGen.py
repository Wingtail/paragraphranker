import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence
import pickle
import gzip
import random
import dataPrep
import blosc

class DataGenerator(Sequence):
    def __init__(self, dataDirectory, batch_size=64, dim=(32,32)): #default values
        with gzip.open(dataDirectory,'r') as file:
            sentencePairs = pickle.load(file)
        grouping = pickle.load(open("words.dat","rb"))
        self.lookuptable = grouping[1]
        self.sentencePairs = list(sentencePairs)
        self.dim = dim
        self.batchSize = batch_size
        self.batchNum = int(len(self.sentencePairs)/self.batchSize)
        if(len(self.sentencePairs) % self.batchSize != 0):
            self.batchNum += 1
        self.on_epoch_end()

    def __len__(self):
        return self.batchNum

    def __getitem__(self, index):
        # Generate indexes of the batch

        # Generate data
        sent1, sent2, y = self.__data_generation(index)
        # print("sent1 shape: ",sent1.shape)
        # print("sent2 shape: ",sent2.shape)
        # print("y shape: ",y.shape)
        return {"sentence1":sent1, "sentence2":sent2}, y

    def on_epoch_end(self):
        print("shuffling")
        random.shuffle(self.sentencePairs)
        print("shuffled")

    def __data_generation(self, index):

        start = index * self.batchSize
        end = (index+1) * self.batchSize

        if(end > len(self.sentencePairs)):
            end = len(self.sentencePairs)

        sent1 = np.zeros((end-start, *self.dim))
        sent2 = np.zeros((end-start, *self.dim))
        y = np.zeros((end-start), dtype=float)

        for i in range(start, end):
            for j in range(len(self.sentencePairs[i][0])):
                sent1[i-start][j] = self.lookuptable[self.sentencePairs[i][0][j]]
            for j in range(len(self.sentencePairs[i][1])):
                sent2[i-start][j] = self.lookuptable[self.sentencePairs[i][1][j]]
            y[i-start] = self.sentencePairs[i][2]

        return sent1, sent2, y

class FileLoadGen(Sequence):
    def __init__(self, dataDirectory): #default values
        with open(dataDirectory+'info.txt', 'r') as file:
            self.batchNum = int([line.rstrip('\n') for line in file][0])
        self.dataDirectory = dataDirectory
        self.counter = 0
        self.on_epoch_end()

    def __len__(self):
        return self.batchNum

    def __getitem__(self, index):
        # Generate indexes of the batch

        # Generate data
        sent1, sent2, y = self.__data_generation(index)
        # print("sent1 shape: ",sent1.shape)
        # print("sent2 shape: ",sent2.shape)
        # print("y shape: ",y.shape)
        return {"sentence1":sent1, "sentence2":sent2}, y

    def on_epoch_end(self):
        # if(self.counter == 2):
        #     dataPrep.cacheArray('trainSentencePairs.pgz', 'data/training/')
        #     dataPrep.cacheArray('valiSentencePairs.pgz', 'data/validation/')
        #     self.counter = 0
        # else:
        #     self.counter += 1
        pass

    def __data_generation(self, index):

        with open(self.dataDirectory+'sentPair_'+str(index), 'rb') as file:
            arr = pickle.load(file)
            sent1 = np.frombuffer(blosc.decompress(arr[0]), dtype=float)
            sent2 = np.frombuffer(blosc.decompress(arr[1]), dtype=float)
            sent1Batch = int(len(sent1)/(500*300))
            sent2Batch = int(len(sent2)/(500*300))
            sent1 = sent1.reshape(sent1Batch, 500,300)
            sent2 = sent2.reshape(sent2Batch,500,300)
            y = np.frombuffer(blosc.decompress(arr[2]), dtype=float)

        return sent1, sent2, y


class ValidationDataGenerator(Sequence):
    def __init__(self, dataDirectory, batch_size=64, dim=(32,32)): #default values
        with gzip.open(dataDirectory,'r') as file:
            sentencePairs = pickle.load(file)
        grouping = pickle.load(open("words.dat","rb"))
        self.lookuptable = grouping[1]
        self.sentencePairs = list(sentencePairs)
        self.dim = dim
        self.batchSize = batch_size
        self.batchNum = int(len(self.sentencePairs)/self.batchSize)
        if(len(self.sentencePairs) % self.batchSize != 0):
            self.batchNum += 1
        self.on_epoch_end()

    def __len__(self):
        return self.batchNum

    def __getitem__(self, index):
        # Generate indexes of the batch

        # Generate data
        sent1, sent2, y = self.__data_generation(index)
        # print("sent1 shape: ",sent1.shape)
        # print("sent2 shape: ",sent2.shape)
        print("y shape: ",y.shape)
        return {"sentence1":sent1, "sentence2":sent2, "target":y}#, y

    def on_epoch_end(self):
        print("shuffling")
        random.shuffle(self.sentencePairs)
        print("shuffled")
        return

    def __data_generation(self, index):

        start = index * self.batchSize
        end = (index+1) * self.batchSize

        if(end > len(self.sentencePairs)):
            end = len(self.sentencePairs)

        sent1 = np.zeros((end-start, *self.dim))
        sent2 = np.zeros((end-start, *self.dim))
        y = np.zeros((end-start,1), dtype=float)

        for i in range(start, end):
            for j in range(len(self.sentencePairs[i][0])):
                sent1[i-start][j] = self.lookuptable[self.sentencePairs[i][0][j]]
            for j in range(len(self.sentencePairs[i][1])):
                sent2[i-start][j] = self.lookuptable[self.sentencePairs[i][1][j]]
            y[i-start][0] = self.sentencePairs[i][2]

        return sent1, sent2, y
