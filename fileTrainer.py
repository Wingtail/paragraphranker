from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from datetime import datetime
from packaging import version
from tensorflow.python.keras.callbacks import TensorBoard
from tensorflow.python.keras.callbacks import LearningRateScheduler
import math
from transformerBlock import Transformer, Siamese

#from dataLoader import Data, DataLoader
import os.path
from dataGen import FileLoadGen


#train1 = np.memmap('./trainingSentence1.npy', dtype='float', mode='r', shape=(431948,248,300))
#train2 = np.memmap('./trainingSentence2.npy', dtype='float', mode='r', shape=(431948,248,300))
#trainTarget = np.memmap('./trainingTarget.npy', dtype='float', mode='r', shape=(431948))
#vali1 = np.memmap('./validationSentence1.npy', dtype='float', mode='r', shape=(18805,248,300))
#vali2 = np.memmap('./validationSentence2.npy', dtype='float', mode='r', shape=(18805,248,300))
#valiTarget = np.memmap('./validationTarget.npy', dtype='float', mode='r', shape=(18805))

#loader = DataLoader()
#data = loader.loadData()

##train1 = data.sent1Data
# train2 = data.sent2Data
#
# vali1 = data.vali1Data
# vali2 = data.vali2Data
# valiTarget = data.valiTarget
#
# trainTarget = data.trainTarget
maximum = 500

print('maximum: ',maximum)

#print('transferred sentence1 shape: ',sentences1.shape)
#print('transferred sentence2 shape: ',sentences2.shape)

transformer = Transformer(3, 6, 300)

model = Siamese(transformer, maximum, 300)

def schedule(epoch):
    if(epoch < 1):
        lr = 1.0e-4
    else:
        lr = 1.0e-4 * math.exp(-(float(3.0*epoch)))
    print(lr)
    return lr

callback = LearningRateScheduler(schedule)

model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1.0e-4), loss='mae', metrics=['acc'])

#X = np.array([[[0., 0.], [0., 0.], [1, 0], [0, 1], [0, 1]],
              #[[0., 0.], [0, 1], [1, 0], [0, 1], [0, 1]]], dtype=np.float)s

logdir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboardCallback = keras.callbacks.TensorBoard(log_dir=logdir)

slider = 0
params = {
          'dataDirectory':"data/training/",
          }

trainGen = FileLoadGen(**params)
params = {
          'dataDirectory':"data/validation/",
          }
valiGen = FileLoadGen(**params)

# sentences1 = np.zeros((min(200, train1.shape[0]-slider),maximum,300))
# sentences2 = np.zeros((min(200, train2.shape[0]-slider),maximum,300))
# trainLabels = np.zeros(min(200, trainTarget.shape[0]-slider))
#
# sentences1[0:sentences1.shape[0]] = train1[slider:slider+sentences1.shape[0]]
# sentences2[0:sentences2.shape[0]] = train2[slider:slider+sentences2.shape[0]]
# trainLabels[0:trainLabels.shape[0]] = trainTarget[slider:slider+trainLabels.shape[0]]
#
# slider+=sentences1.shape[0]
#
# print('loading dataset..')
# dataset = tf.data.Dataset.from_tensor_slices(({"sentence1":sentences1, "sentence2":sentences2}, trainLabels))
# dataset = dataset.batch(64)

#validataset = tf.data.Dataset.from_tensor_slices(({"sentence1":vali1, "sentence2":vali2}, valiTarget))
#validataset = validataset.batch(64)

#dataset = dataset.repeat()
# dataset = dataset.shuffle(buffer_size= (int(sentences1.shape[0]*0.4) + 3 * 64))
print('loaded')

model.fit_generator(generator=trainGen, validation_data=valiGen, use_multiprocessing=False, epochs=5, callbacks=[tensorboardCallback, callback]) #, validation_data=validataset

print('saving')

transformer.save_weights('./checkpoint/transformer_best')

print('done')
# print('loading')

# transformer = Transformer(3,6,300)
# transformer.load_weights('./checkpoint/transformerr')
#
# model = Siamese(transformer)
# model.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='mse', metrics=['acc'])
# print('evaluating...')
#
# loss, acc = model.evaluate(dataset)
# print(transformer.predict(sentences1))
#
# print('restored model: acc: ',acc, 'loss: ',loss)


#print(model.summary())
#print("prediction!!: ",model.predict([data, data2]))
