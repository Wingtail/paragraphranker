import tensorflow as tf
import gzip
import pickle
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np


##This method is for FeatureList
# def sentToTF(sentData):
#     sentTF = []
#
#     total = float(len(sentData))
#     count = 0
#     for sentence in sentData:
#         count+=1
#         sentTF.append(tf.train.Feature(int64_list=tf.train.Int64List(value=np.array(sentence))))
#         print(count/total*100,'% done')
#         if(count == 100):
#             break
#     #sentence = tf.train.Feature(int64_list=tf.train.Int64List(value=sentData))
#     sentence = tf.train.FeatureList(feature=sentTF)
#     return sentence

def writeTfRecord(directory, pair1, pair2, target):
    sampleSize = len(pair1)
    print('writing to ', directory)
    with tf.io.TFRecordWriter(directory) as writer:
        for i in range(sampleSize):
            sentence1 = tf.train.Feature(int64_list=tf.train.Int64List(value=pair1[i]))
            sentence2 = tf.train.Feature(int64_list=tf.train.Int64List(value=pair2[i]))
            label = tf.train.Feature(float_list=tf.train.FloatList(value=[target[i]]))
            sentencePairs = {'sentence1':sentence1, 'sentence2':sentence2, 'label':label}
            features = tf.train.Features(feature=sentencePairs)
            example = tf.train.Example(features=features)
            writer.write(example.SerializeToString())
            print(float(i)/sampleSize*100,'% done')

    print(directory,' complete')

with gzip.open('train1.pgz','r') as file:
    sent1Data = pickle.load(file)
    #sent1Data = pad_sequences(sent1, 300, padding='post')
    print(len(sent1Data))
with gzip.open('train2.pgz','r') as file:
    sent2Data = pickle.load(file)
    #sent2Data = pad_sequences(sent2, 300, padding='post')

with gzip.open('trainLabel.pgz','r') as file:
    trainTarget = pickle.load(file)

with gzip.open('vali1.pgz','r') as file:
    vali1Data = pickle.load(file)
    #vali1Data = pad_sequences(sent1, 300, padding='post')

with gzip.open('vali2.pgz','r') as file:
    vali2Data = pickle.load(file)
    #vali2Data = pad_sequences(sent2, 300, padding='post')

with gzip.open('valiLabel.pgz','r') as file:
    valiTarget = pickle.load(file)


print('writing sent1 training')
sent1 = pad_sequences(sent1Data,300,padding='post')
print('writing sent2 training')
sent2 = pad_sequences(sent2Data,300,padding='post')
print('writing label training')

print('writing sent1 training')
vali1 = pad_sequences(vali1Data,300,padding='post')
print('writing sent2 training')
vali2 = pad_sequences(vali2Data,300,padding='post')
print('writing label training')

# print('writing sent1 validation')
# vali1 = sentToTF(pad_sequences(vali1Data,300,padding='post'))
# print('writing sent1 validation')
# vali2 = sentToTF(pad_sequences(vali1Data,300,padding='post'))
# print('writing labels validation')
# valiLabels = tf.train.FeatureList(feature=[tf.train.Feature(float_list=tf.train.FloatList(value=valiTarget))])
#'trainSentencePairs.tfrecord'

# writeTfRecord('trainSentencePairs.tfrecord', sent1, sent2, trainTarget)
# writeTfRecord('valiSentencePairs.tfrecord', vali1, vali2, valiTarget)

# print('constructing train pair')
# trainPair = tf.train.FeatureLists(feature_list=sentencePairs)
# print('constructing validation pair')
# valiPair = tf.train.FeatureLists(feature_list=valiPairs)
#
# print('constructing train example')
# trainExample = tf.train.SequenceExample(feature_lists=trainPair)
# print('constructing vali example')
# valiExample = tf.train.SequenceExample(feature_lists=valiPair)
