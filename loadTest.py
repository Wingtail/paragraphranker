import numpy as np
import pickle
import blosc

with open('data/training/sentPair_1', 'rb') as file:
    a = pickle.load(file)
    arb = np.frombuffer(blosc.decompress(a[0]), dtype=float)
    size = len(arb)
    batch = int(size/(500*300))
    print(np.frombuffer(blosc.decompress(a[0]),dtype=float).reshape(batch, 500,300).shape)
