import tensorflow as tf
import json_lines
import re
import numpy as np
import random
import csv
import pickle
import nltk
import gzip
#import chars2vec


class DataLoader:
    def __init__(self):
        self.vocabulary = {}
        self.trainSentencePairs = [] #tuple format (sent1, sent2, target) in word vector format
        self.valiSentencePairs = []

        self.trainTarget = []
        self.sent1Data = []
        self.sent2Data = []

        self.vali1Data = []
        self.vali2Data = []
        self.valiTarget = []

        self.embedding = None
        self.oov = []
        self.count = 0
        self.batchSize = 100
        self.sample = 0
        self.maxSamples = 100000

        self.lookuptable = []

        self.maximum = 512 #assume that there can be no more than 512 words in 1 sentence

    '''
    loadSentencePair methods are not meant to be generalized dataset loaders.
    They are meant specifically for loading the snli, quora dataset.
    To override the method to load other datasets, manually refer to specific labels to retrieve desired data-entry.
    '''

    def loadSentencePairJSON(self, directory, boundary, isVali):
        label = {}
        label['neutral'] = 0.5
        label['contradiction'] = 0.0
        label['entailment'] = 1.0
        print('loading sentence pairs')

        sample = 0

        with open(directory, 'rb') as f:
            for item in json_lines.reader(f):
                sent1 = item['sentence1'].lower()
                sent2 = item['sentence2'].lower() #Make case insensitive
                if(isVali==False):
                    t = np.mean([label[l] for l in item['annotator_labels'] if l in label])
                else:
                    if(item['gold_label'] in label):
                        t = label[item['gold_label']]
                    else:
                        continue

                words1 = nltk.word_tokenize(sent1)
                words1 = [word for word in words1 if len(word) > 0]
                sent1Index = []
                for word in words1:
                    if(word not in self.vocabulary):
                        self.vocabulary[word] = self.count
                        self.count += 1
                        #going to use character encoder model --> better than random representation
                        #however, consider discrepencies with original word vectors
                        self.oov.append(np.random.random(300))
                    sent1Index.append(self.vocabulary[word])
                words2 = nltk.word_tokenize(sent2)
                words2 = [word for word in words2 if len(word) > 0]
                sent2Index = []
                for word in words2:
                    if(word not in self.vocabulary):
                        self.vocabulary[word] = self.count
                        self.count += 1
                        self.oov.append(np.random.random(300))
                    sent2Index.append(self.vocabulary[word])
                #pair = (tuple(sent1Index), tuple(sent2Index), t) #list to tuple for memory compression

                #print('in progress')
                if(sample < boundary):
                    if(len(sent1Index) > 0 and len(sent2Index) > 0):
                        if(isVali == False):
                            self.sent1Data.append(sent1Index)
                            self.sent2Data.append(sent2Index)
                            self.trainTarget.append(t)
                        else:
                            self.vali1Data.append(sent1Index)
                            self.vali2Data.append(sent2Index)
                            self.valiTarget.append(t)
                        print(float(sample)/boundary * 100.0,"% done")
                        sample += 1
                else:
                    print('sentence pairs loaded')
                    return

            print('sentences loaded')

    #The Quora question pair dataset does not have dev.
    #Directly distributes approximately 10% of the data to validation set



    def loadSentencePairCSV(self, directory, valiProb, boundary): #proportion to allocate to validation
        csvReader = csv.reader(open(directory,'r'),delimiter=',')

        lineCount = 0
        sample = 0

        for row in csvReader:
            if(lineCount > 0):
                sent1 = row[3].lower()
                sent2 = row[4].lower() #make case insensitive
                t = float(row[5])
                words1 = nltk.word_tokenize(sent1)
                words1 = [word for word in words1 if len(word) > 0]
                sent1Index = []
                for word in words1:
                    if(word not in self.vocabulary):
                        self.vocabulary[word] = self.count
                        self.count += 1
                        self.oov.append(np.random.random(300))
                    sent1Index.append(self.vocabulary[word])
                words2 = nltk.word_tokenize(sent2)
                words2 = [word for word in words2 if len(word) > 0]
                sent2Index = []
                for word in words2:
                    if(word not in self.vocabulary):
                        self.vocabulary[word] = self.count
                        self.count += 1
                        self.oov.append(np.random.random(300))
                    sent2Index.append(self.vocabulary[word])
                #pair = (tuple(sent1Index), tuple(sent2Index), t)
                #print('in progress')
                if(sample >= boundary):
                    return

                if(len(sent1Index) > 0 and len(sent2Index) > 0):
                    #self.trainSentencePairs.append(pair)
                    self.sent1Data.append(sent1Index)
                    self.sent2Data.append(sent2Index)
                    self.trainTarget.append(t)
                    print(float(sample)/boundary * 100.0,"% done")
                    sample += 1
            lineCount += 1

    # def convertTorchBatches(self, sentencePairs, directory, batchSize):
    #     sent1Data = None
    #     sent2Data = None
    #     targets = None
    #
    #     array = []
    #     print('got maximum: ', self.maximum)
    #
    #     maxCount = 1000
    #
    #     idBatch = 0
    #     indexx = 0
    #     for i in range(0,len(sentencePairs)):
    #         if(indexx%batchSize == 0):
    #             print('evaluating batch ',idBatch)
    #             if(sent1Data is not None):
    #                 sent1Directory = './'+directory+'Sentence1_'+str(idBatch)+'.npy'
    #                 sent2Directory = './'+directory+'Sentence2_'+str(idBatch)+'.npy'
    #                 targetDirectory = './'+directory+'Target_'+str(idBatch)+'.npy'
    #                 np.save(sent1Directory, sent1Data)
    #                 np.save(sent2Directory, sent2Data)
    #                 np.save(targetDirectory, targets)
    #                 idBatch+=1
    #
    #             sent1Data = np.zeros((batchSize, self.maximum, len(self.lookuptable[0])))
    #             sent2Data = np.zeros((batchSize, self.maximum, len(self.lookuptable[0])))
    #             targets = np.zeros(batchSize)
    #
    #         #print('loading array...')
    #         sent1 = np.asarray([self.lookuptable[index] for index in sentencePairs[i][0]])
    #         sent2 = np.asarray([self.lookuptable[index] for index in sentencePairs[i][1]])
    #         #print('sent1: ',sent1.shape)
    #         #print('sent2: ',sent2.shape)
    #         #print('loaded')
    #         if(len(sent1) > 0 and len(sent2 > 0)):
    #             sent1 = np.stack(sent1)
    #             sent2 = np.stack(sent2)
    #             #print('stacked')
    #             #padding
    #
    #             sent1Data[indexx%batchSize][:sent1.shape[0],:sent1.shape[1]] = sent1
    #             sent2Data[indexx%batchSize][:sent2.shape[0],:sent2.shape[1]] = sent2
    #             #print('sent1Data: ', sent1Data[i])
    #             #print('sent2Data: ', sent2Data[i])
    #             targets[indexx%batchSize] = sentencePairs[i][2]
    #             indexx += 1
    #         else:
    #             print('some weired shit..')
    #     print('converting to array')
    #
    #     print('converted')
    #     print('sentence1Data shape: ', sent1Data.shape)
    #     print('sentence2Data shape: ', sent2Data.shape)
    #     print('target shape: ', targets.shape)
    #
    #     return idBatch

    def loadData(self):
        f = open('./datasets/glove.6B/glove.6B.300d.txt', "r")
        self.count = 1
        print('loading glove vectors')
        lines = f.readlines(204800)
        tensors = []
        while lines:
            for line in lines:
                index = line.index(' ')
                values = line[index+1:]
                key = line[:index]
                tensors.append(np.fromstring(values, dtype=np.float32, sep=' '))
                self.vocabulary[key] = self.count
                self.count += 1
            lines = f.readlines(204800)
        print('data loaded')

        weights = np.zeros((len(tensors), len(tensors[0])))

        for i in range(len(tensors)):
            weights[i] = tensors[i]
        # weights = np.stack(tensors) # --> too inefficient

        print('loading quora questions')
        self.loadSentencePairCSV('./datasets/questions.csv',0.1, 300000)
        print('loaded!')
        print('loading snli training set')
        self.loadSentencePairJSON('./datasets/snli_1.0/snli_1.0_train.jsonl' , 600000, False)
        print('loaded!')
        print('loading dev sentence pairs')
        self.loadSentencePairJSON('./datasets/snli_1.0/snli_1.0_dev.jsonl', 50000, True)
        print('loaded!')

        if(len(self.oov)>0):

            newWeights = np.zeros((len(self.oov)+len(weights)+1, len(weights[0])))

            for i in range(1,len(weights)+1):
                newWeights[i] = weights[i-1]

            for i in range(len(weights)+1, len(newWeights)):
                newWeights[i] = self.oov[i-(len(weights)+1)]

        print('put weights into embedding')
        self.lookuptable = newWeights

        #self.sentencePairs.sort(key=lambda x:len(x[0]))
        #self.valiSentencePairs.sort(key=lambda x:len(x[0]))
        #print('sorted')
        #print('converting into tensor word vectors')

        # self.trainIdBatch = self.convertTorchBatches(self.sentencePairs, 'data/training/', 100)
        # self.valiIdBatch = self.convertTorchBatches(self.valiSentencePairs, 'data/validation/', 100)

        print('data converted')
        print('dumping self.vocabulary and self.embedding')
        grouping = (self.vocabulary, self.lookuptable)
        print("writing self.vocabulary")

        file = open("words.dat","wb")
        pickle.dump(grouping, file)
        file.close()

        # print("complete")
        # print("converting sentencePairs to tuples")
        # self.trainSentencePairs = tuple(self.trainSentencePairs)
        # self.valiSentencePairs = tuple(self.valiSentencePairs)
        # print("done")
        # print("writing trainSentencePairs in gzip")
        #file = open("trainSentencePairs.dat", "wb")



        with gzip.GzipFile('train1.pgz', "w") as file:
            pickle.dump(self.sent1Data, file)
        with gzip.GzipFile('train2.pgz', "w") as file:
            pickle.dump(self.sent2Data, file)
        with gzip.GzipFile('trainLabel.pgz', "w") as file:
            pickle.dump(self.trainTarget, file)
        #file.close()
        #file = open("valiSentencePairs.dat", "wb")
        print("writing valiSentencePairs in gzip")
        # with gzip.GzipFile('valiSentencePairs.pgz', "w") as file:
        #     pickle.dump(self.valiSentencePairs, file)

        with gzip.GzipFile('vali1.pgz', "w") as file:
            pickle.dump(self.vali1Data, file)
        with gzip.GzipFile('vali2.pgz', "w") as file:
            pickle.dump(self.vali2Data, file)
        with gzip.GzipFile('valiLabel.pgz', "w") as file:
            pickle.dump(self.valiTarget, file)

        #file.close()

        print('complete')
        #print("complete")

loader = DataLoader()
loader.loadData()
