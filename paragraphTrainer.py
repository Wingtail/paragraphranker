import tensorflow as tf
import pickle
from tensorflow import contrib
import numpy as np
import math
from transformerBlock import Transformer, ParagraphSiamese
import random
from datetime import datetime
from tensorflow import keras
from tensorflow.python.keras.callbacks import TensorBoard
from paragraphDataGen import DataGenerator
import gzip
from tensorflow.python.keras.callbacks import LearningRateScheduler

f = gzip.GzipFile('processedData/paragraphBatch.npy.gz', "r")
print('loading file')
paragraphBatch = np.load(f)
f = gzip.GzipFile('processedData/queryBatch.npy.gz', "r")
print('loading file')
queryBatch = np.load(f)
mapping = pickle.load(open('processedData/mapFile',"rb"))


f = gzip.GzipFile('processedData/validation_paragraphBatch.npy.gz', "r")
print('loading file')
vali_paragraphBatch = np.load(f)
f = gzip.GzipFile('processedData/validation_queryBatch.npy.gz', "r")
print('loading file')
vali_queryBatch = np.load(f)
vali_mapping = pickle.load(open('processedData/validation_mapFile',"rb"))


paragraphBatch = paragraphBatch.astype(np.float32)
queryBatch = queryBatch.astype(np.float32)

vali_paragraphBatch = vali_paragraphBatch.astype(np.float32)
vali_queryBatch = vali_queryBatch.astype(np.float32)


def schedule(epoch):
    lr = 1e-6 * math.exp(-(float(epoch)))
    print(lr)
    return lr

callback = LearningRateScheduler(schedule)
#model should be Siamese
transformer = Transformer(3, 6, 300)
model = ParagraphSiamese(transformer, paragraphBatch.shape[1])
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1.0e-6), loss='mae', metrics=['acc'])

logdir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboardCallback = keras.callbacks.TensorBoard(log_dir=logdir)


params = {'dim': (50,300),
          'queryBatch':queryBatch,
          'paragraphBatch':paragraphBatch,
          'mapping':mapping}

trainGen = DataGenerator(**params)

params = {'dim': (50,300),
          'queryBatch':vali_queryBatch,
          'paragraphBatch':vali_paragraphBatch,
          'mapping':vali_mapping}

valiGen = DataGenerator(**params)

print('loaded')

model.fit_generator(generator=trainGen, validation_data=valiGen, use_multiprocessing=False, epochs=5, callbacks=[tensorboardCallback, callback]) #, validation_data=validataset

transformer.save_weights('./checkpoint/paragraphTransformer')
print('saved')
#paragraphBatch is the batch

#model architecture
